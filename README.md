﻿# MED-VS

MED-VS stands for Marjuana Enforcement Division - Validation & Scripting. The main goal of MED-VS is to make it easy to create, deploy, and update validation and format conversion processes at my current place of work. This project uses various Python modules, including OpenPyXL and XLRD to parse and validate agricultural data often stored in an '.xlsx'. The validated data is then used to generate an Excel report and '.csv' files. It’s intended to be a pretty specific library to help with data validation in preparation for upload to METRC in compliance with Colorado MED regulated facilities.

This repository is intended to make troubleshooting and maintaining the scripts stored at work easier while I am away. It will also keep some personal files synced, such as the .vimrc.

Imported using 'medvs'.

## Contact

mr.carpinelli@protonmail.ch

## Building

The code files provided are written as a Python package to be used by more task-oriented scripts. The scripts use Python3, the OpenPyXl module, XLRD, Microsoft batch files, and git for the command prompt.

## Contributing

This is mostly a pretty specific personal project, but any contributions are welcome and appreciated. Forks and clones are also welcome, provided the [LICENSE](LICENSE) file is kept.

## License

All contributions are made under the [GNU General Public License v3](https://www.gnu.org/licenses/gpl-3.0.en.html). See the [LICENSE](LICENSE).

