" the call to :runtime you can find below.  If you wish to change any of
" those settings, you should do it in this file (/etc/vim/vimrc), since
" debian.vim will be overwritten everytime an upgrade of the vim packages
" is performed. It is recommended to make changes after sourcing
" debian.vim since it alters the value of the 'compatible' option.

" This line should not be removed as it ensures that various options are
" properly set to work with the Vim-related packages available in Debian.
runtime! debian.vim

" Vim will load $VIMRUNTIME/defaults.vim if the user does not have a vimrc.
" This happens after /etc/vim/vimrc(.local) are loaded, so it will override
" any settings in these files.
" If you don't want that to happen, uncomment the below line to prevent
" defaults.vim from being loaded.
" let g:skip_defaults_vim = 1

" Uncomment the next line to make Vim more Vi-compatible
" NOTE: debian.vim sets 'nocompatible'.  Setting 'compatible' changes
" numerous options, so any other options should be set AFTER setting
" 'compatible'.
"set compatible

" Vim5 and later versions support syntax highlighting. Uncommenting the next
" line enables syntax highlighting by default.
if has("syntax")
  syntax on
endif

" If using a dark background within the editing area and syntax highlighting
" turn on this option as well
"set background=dark

" Uncomment the following to have Vim jump to the last position when
" reopening a file
"if has("autocmd")
"  autocmd BufReadPost * if line("'\"") > 1 && line("'\"") <= line("$") | exe "normal! g'\"" | endif
"endif

" Uncomment the following to have Vim load indentation rules and plugins
" according to the detected filetype.
if has("autocmd")
  filetype plugin indent on
endif

" The following are commented out as they cause vim to behave a lot
" differently from regular Vi. They are highly recommended though.
set showcmd   " Show (partial) command in status line.
set showmatch   " Show matching brackets.
set ignorecase    " Do case insensitive matching
set smartcase   " Do smart case matching
set incsearch   " Incremental search
set autowrite   " Automatically save before commands like :next and :make
set hidden    " Hide buffers when they are abandoned
set mouse=a   " Enable mouse usage (all modes)

" Source a global configuration file if available
if filereadable("/etc/vim/vimrc.local")
  source /etc/vim/vimrc.local
endif


" ######################################################################## #
" ######################################################################## #
" ######################################################################## #
" ######################################################################## #


" Vimscript without Plugins

" No need for backwards compatibility with Vi
set nocompatible
" Enable syntax and plugins
syntax enable
filetype plugin indent on
" Recursively search for file related commands
set path+=**
" List possible files upon tab-completion
set wildmenu
" Create a 'tags' file


" ######################################################################## #
" ######################################################################## #
" ######################################################################## #
" ######################################################################## #


" Personal

" Functions
function! ShowWhitespace()
  echo "eol='¬', tab='>·', trail='~', extends='>', precedes='<', space='·'"

  set listchars=eol:¬,tab:>·,trail:~,extends:>,precedes:<,space:·
  set list!
endfunction

function! RunFile()
  call inputsave()
  let filename = input("Run filename: ")
  call inputrestore()

  let name_splits = split(filename, '\.')
  if len(name_splits) > 1
    if name_splits[1] == "py"
      let runtext = "python3 " . filename

    else
      let error_text = "echo \"\.vimrc not setup to run this type of file\""
      echo error_text
      let runtext = error_text
    endif

  elseif len(name_splits) == 1
    let runtext = "./" . filename
  else
    let error_text = "echo \"\.vimrc not setup to run this type of file\""
    echo error_text
    let runtext = error_text
  endif

  execute "!" . runtext
endfunction

" Remaps
" Commandmode Remaps
nnoremap <leader>w :call ShowWhitespace()<CR>
nnoremap <leader>r :call RunFile()<CR>
nnoremap <leader>m :!clear && make<CR>
nnoremap <leader>p :lprevious<CR>
nnoremap <leader>n :lnext<CR>
" Normalmode Remaps
nnoremap <leader>O O<Esc>
nnoremap <leader>o o<Esc>
nnoremap <leader>t gg=G<CR>

" Saved commands
" 
" Sets
" set number
" set smartindent
"
" set list  " built-in whitespace shower
" :r! sed -n 147,227p /path/to/foo/foo.c # Change line number range
" Autocmd
" autocmd filetype python setlocal ts=4 sw=4 sts=0 expandtab
" retab
" colorscheme delek
" Create/Append to ~/.inputrc
" set editing-mode vi


" ######################################################################## #
" ######################################################################## #
" ######################################################################## #
" ######################################################################## #


" Python Setup

" File List
" ^([^.]+)$  " for files containing no '.'s
" ^[^.].*$  " for dotfiles
" ^.*  " simpler dotfiles
" */.*,  " absolute path dotfiles
" *.c, *.cpp, *.h, *.hpp
" *.py, *.pyw, *.pyc
" *.js, *.html, *.css
" Sets
set nocompatible
set splitbelow
set splitright
set encoding=utf-8
" Enable folding
" The initial command, set foldmethod=indent,
" creates folds based upon line indents.
set foldmethod=indent
set foldlevel=99
" Make code 'look pretty'
let python_highlight_all = 1
syntax on


" Autocmd
" To add the proper PEP 8 indentation, add the following to your .vimrc:
" " There is also a plugin called ftypes that will allow you to have a 
" separate file for each filetype you want to maintain settings for, 
" so use that if you see fit.
autocmd BufNewFile,BufRead *.py
  \ set number |
  \ set tabstop=4 |
  \ set softtabstop=4 |
  \ set shiftwidth=4 |
  \ set textwidth=75 |
  \ set expandtab |
  \ set autoindent |
  \ set fileformat=unix  " |
  " \ retab

autocmd BufNewFile,BufRead ^.*,*/.*,*.c,*.cpp,*.h,*.hpp,*.js,*.html,*.css
  \ set number |
  \ set tabstop=2 |
  \ set softtabstop=2 |
  \ set shiftwidth=2 |
  \ set textwidth=75 |
  \ set expandtab |
  \ set autoindent |
  \ set fileformat=unix  " |
  " \ retab

" This will mark extra whitespace as bad and probably color it red.
highlight BadWhitespace ctermbg=red guibg=red
autocmd BufRead,BufNewFile *.py,*pyw,^.*,*/.*,*.c,*.cpp,*.h,*.hpp,*.js,*.html,*.css match BadWhitespace /\s\+$/


" Remaps
" split navigations
nnoremap <C-J> <C-W><C-J>
nnoremap <C-K> <C-W><C-K>
nnoremap <C-L> <C-W><C-L>
nnoremap <C-H> <C-W><C-H>


" Plugins
" Vundle
filetype off
" Set the runtime path to include Vundle and initialize
set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()
" alternatively, pass a path where Vundle should install plugins
" cal vundle#begin('~/some/path/here')
" let Vundle manage Vundle, required
Plugin 'gmarik/Vundle.vim'
" add all your plugins here (note older versions of Vundle
" use Bundle instead of Plugin
" All of your Plugins must be added before the following line
call vundle#end()
filetype plugin indent on
" EndVundle
" Code Management
Plugin 'vim-syntastic/syntastic'
Plugin 'nvie/vim-flake8'
Plugin 'tmhedberg/SimpylFold'
" Code Utilities
Bundle 'Valloric/YouCompleteMe'
Plugin 'vim-scripts/indentpython.vim'
" File Browser
Plugin 'scrooloose/nerdtree'
Plugin 'jistr/vim-nerdtree-tabs'
Plugin 'kien/ctrlp.vim'
" Git
Plugin 'tpope/vim-fugitive'
" UI & Colorschemes
" Powerline is a status bar that displays the current virtualenv,
" git branch, files being edited, and much more.
Plugin 'Lokaltog/powerline', {'rtp': 'powerline/bindings/vim/'}
Plugin 'jnurmine/Zenburn'
Plugin 'altercation/vim-colors-solarized'
" Install with :PluginInstall


" Plugin Setup
" SimplyFold Setup
" See the docstrings for folded code
let g:SimplyFold_docstring_preview = 1
" YouCompleteMe (YCM) Setup
" The first line ensures that the auto-complete window goes away when 
" you’re done with it, and the second defines a shortcut for goto 
" definition. 
let g:ycm_autoclose_preview_window_after_completion=1
map <leader>g  :YcmCompleter GoToDefinitionElseDeclaration<CR>
" Syntastic Setup
set statusline+=%#warningmsg#
set statusline+=%{SyntasticStatuslineFlag()}
set statusline+=%*
let g:syntastic_always_populate_loc_list = 1
let g:syntastic_auto_loc_list = 1
let g:syntastic_check_on_open = 1
let g:syntastic_check_on_wq = 0
" let g:syntastic_<filetype>_checkers = ['<checker-name>']
let g:syntastic_python_checkers = ['flake8']
" Zenburn & Solarize Setup
" Color schemes work in conjunction with the basic color schemes
" solarized for GUI mode, Zenburn for terminal mode
if has('gui_running')
  set background=dark
  colorscheme solarized
else
  colorscheme zenburn
endif
" Solarized also ships with a dark and light theme switch between them
" with F5
call togglebg#map("<F5>")
" NERDTree Setup
" auto-open NERDTree
autocmd VimEnter * NERDTree
map <C-n> :NERDTreeToggle<CR> " Toggle NERDTree with Ctrl + n
" Close vim if only window left is NERDTree
autocmd bufenter * if (winnr("$") == 1 && exists("b:NERDTree") && b:NERDTree.isTabTree()) | q | endif
" Want to hide .pyc files? Then add the following line:
let NERDTreeIgnore=['\.pyc$', '\~$'] " ignore files in NERDTree


" Saved Commands
" Enable folding with the spacebar
" nnoremap <space> za
"
" My leader key is mapped to space, so space-g will goto definition of 
" whatever I’m currently on. That’s helpful when I’m exploring new code.
"
" Access system clipboard
" set clipboard=unnamed
" 
" This determines if you are running inside a virtualenv, switches to 
" that specific virtualenv, and then sets up your system path so that 
" YouCompleteMe will find the appropriate site packages.
" python with virtualenv support
" python3 << EOF
" import os
" import sys
" if 'VIRTUAL_ENV' in os.environ:
"   project_base_dir = os.environ['VIRTUAL_ENV']
"   activate_this = os.path.join(project_base_dir, 'bin/activate_this.py')
"   exec(compile(open(activate_this, "rb").read(), activate_this, 'exec'),
"        dict(__file__=activate_this))
"   # execfile(activate_this, dict(__file__=activate_this))
" EOF
