"""templates module."""

import re


""" Constants """
# Sets #
Blanks = (None, "")
Cycle_Range = tuple([cycle for cycle in range(1, 15)])

Spelled_Digits = {0: "zero", 1: "one", 2: "two", 3: "three", 4: "four",
                  5: "five", 6: "six", 7: "seven", 8: "eight", 9: "nine"}
Spelled_Words = {"DR.": "Doctor", "DR": "Doctor"}

# GetStrain Set
# End Sets #

# Strings
Units = "Grams"
Not_Found = "NOT FOUND"
No_Tag = "NO TAG"

# Formatting
Date_Format = "%m{Separator}%d{Separator}%Y"
Batchname_Format = "{date} {State_Code} {strain_code}"
Cycle_Format = "DRYING ROOM #{Cycle}"

# Regular Expressions
Room_Pattern = re.compile(r"([A-S])-(\d{1,2})")  # COMMENT ME
Strain_Pattern = re.compile(r"(^\S.*?)(?=:|$)")  # COMMENT ME

# Column Indices
RFID_Index = 0
Weight_Index = 1
Strain_Index = 2
Room_Index = 3

# Data Ranges
Start_Row = 3
End_Row = 1000
Start_Column = RFID_Index + 1
End_Column = Room_Index + 1
""" End Constants """
