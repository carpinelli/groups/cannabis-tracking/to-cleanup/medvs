"""Core medvs module"""

from operator import itemgetter

from medvs import utils
from medvs import templates


""" Functions """


def isNotFound(Info):
    """Docstring"""
    if Info == "Not Found":
        return True
    else:
        return False


def isInRange(Item, Range):
    if Item in Range:
        return True
    else:
        return False


def isInSet(Item, Set):
    """Docstring"""
    if Item not in Set:
        return False
    else:
        return True


def getBatchName(strain):
    """Docstring"""
    Wednesdays_Date = utils.getWednesdaysDate()

    Strain_Words = strain.split(' ')
    Converted_Strain_Words = list()

    for word in Strain_Words:
        if word in templates.Spelled_Digits.keys():
            # if word is 0-9
            word = utils.getWordFromNumber(word)

        Converted_Strain_Words.append(word[:2])

    out_batchname = (Wednesdays_Date + ' ' + ' '
                     + "".join(Converted_Strain_Words))

    return out_batchname


def getUppercaseSheetlist(in_Sheetlist):
    """Takes an openpyxl Workbook file and cell location information
    Capitalizes the range of cells specified in the argument parameters
    Returns a string containing the path to the capitalized file
    """
    # logger.critical("Row len (uppercase before): %s"
    #                 % (str(len(in_Sheetlist))))
    out_sheetlist = list()  # create return variable at scope

    for row_number, row in enumerate(in_Sheetlist, start=1):
        uppercase_row = list()
        # list to hold capitalized rows before appending

    for column_number, cell_value in enumerate(row, start=1):
        if cell_value is None:
            cell_value = ""

        # capitalize all characters
        # logger.info("Changing %s to uppercase..."
        #           % (get_column_letter(column_number)
        #              + str(row_number)))
        uppercase_row.append(str(cell_value).upper())

        # logger.info("Changed %s to %s"
        # % (get_column_letter(column_number)
        #    + str(row_number),
        #    uppercase_row[column_number - 1]))

    out_sheetlist.append(uppercase_row)

    # logger.critical("Row len (uppercase after): %s"
    #                 % (str(len(out_sheetlist))))

    return out_sheetlist


def getSortedSheetlist(in_row_list):
    """Returns a sorted 2D list"""
    # logger.info("Sorting by RFID as tertiary sort...")

    # tertiary sort by RFID, as new list
    out_sorted_rows = sorted(in_row_list, key=itemgetter(templates.RFID_Index))

    # Sorting by room

    # logger.info("Sorting by room...")

    # secondary sort, reversed
    # logger.info("Sorting by room number as secondary sort...")
    out_sorted_rows.sort(key=utils.f_getRoomNumber)

    # primary sort by room letter
    # logger.info("Sorting by room letter as primary sort...")
    out_sorted_rows.sort(key=utils.f_getRoomLetter)

    return out_sorted_rows


""" End Functions """
